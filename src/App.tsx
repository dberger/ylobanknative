import React from 'react';
import {Provider} from 'react-redux';
import Store from './store/configureStore';
import {AppContextProvider} from './store/hooks/AppContext';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {ApplicationProvider, IconRegistry} from '@ui-kitten/components';
import {EvaIconsPack} from '@ui-kitten/eva-icons';
import {dark as darkTheme, mapping} from '@eva-design/eva';
import LoginScreen from './screens/login/LoginScreen';
import DashboardScreen from './screens/dashboard/DashboardScreen';

const MainNavigator = createStackNavigator({
    Login: {
        screen: LoginScreen,
        navigationOptions: {
            title: 'Home',
            headerShown: false
        }},
    Dashboard: {
        screen: DashboardScreen,
        navigationOptions: {
            headerTitle: 'Tableau de bord'
        }
    }
});

const AppNavigatorContainer = createAppContainer(MainNavigator);

export default function App() {
    return (
        <AppContextProvider>
            <Provider store={Store}>
                <IconRegistry icons={EvaIconsPack}/>
                <ApplicationProvider mapping={mapping} theme={darkTheme}>
                    <AppNavigatorContainer/>
                </ApplicationProvider>
            </Provider>
        </AppContextProvider>
    );
}
