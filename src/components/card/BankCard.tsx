import React from 'react';
import {
  Image,
  StyleSheet,
  View,
  TouchableOpacity,
} from 'react-native';
import {
  Card,
  Text,
  Icon,
  CardHeader,
  Layout,
} from '@ui-kitten/components';

interface BankCardProps {
	item: Account,
	onShowDetails(account: Account): void
}

let item;

export const BankCardHeader = () =>  (
	<CardHeader title={item.type}/>
);



export const BankCard = (props: BankCardProps) => {
	item = props.item;
	const {onShowDetails} = props;
	const _onPressButton = (e) => onShowDetails(props.item)

    return (
 
  <TouchableOpacity
  onPress={_onPressButton}>

  <Card header={BankCardHeader} status='success'>
	  <View style={styles.cardBody}>
  <View style={styles.accountSummary}>
        <Icon name={item.codeType === 'cpt_cheque' ? 'credit-card-outline' : 'trending-up'} style={styles.leftIcon}  fill='green'/>
		<View style={styles.subContainer}>
			<Text category='h1'style={styles.textCard} >{item.solde} €</Text>
			<Icon onPress={_onPressButton} name='arrow-forward' style={styles.arrowIcon}  fill='#3366FF'/>
		</View>
    </View>
	</View>
  </Card>
</TouchableOpacity>
)};

const styles = StyleSheet.create({
	subContainer: {
		flex: 1,
		alignItems: "flex-end",
	},
	arrowIcon : {
		marginTop: 15,
		width:32,
		height: 32
	},
	leftIcon : {
		width:128,
		height: 128
	},
	textCard : {
		color: 'green',
	},
	cardBody: {
		height: 100
	},
    accountSummary: {
        flex: 1,
        flexDirection: "row",
        flexWrap: "wrap",
		alignContent: "space-around",
		alignItems: "center",
		justifyContent: "space-between"
    }, 
    headerText: {
        marginHorizontal: 24,
        marginVertical: 16,
    },
    headerImage: {
        flex: 1,
        height: 192,
  },
});
