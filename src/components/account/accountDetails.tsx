import React, {useContext, useEffect, useState} from 'react';
import {Text} from '@ui-kitten/components';
import {StyleSheet, View} from 'react-native'
import { NavigationScreenProp, NavigationState, NavigationParams } from 'react-navigation';
import {
    List,
    ListItem,
  } from '@ui-kitten/components';
  
interface Props{
    account: Account;
}

const AccountDetails = (props : Props) => {
    const {account} = props;

    const renderItem = ({ item, index }) => {
        const renderAccessory = (style, index) => (
        <Text style={style}>{`${account.transactions[index].amount} €`}</Text>
          );
        return (
        
<ListItem
      title={item.type}
      description='02/02/2020 18:38'
      accessory={renderAccessory}
    />      );
        }
     

 
      return (
        <List
          data={account.transactions.reverse()}
          renderItem={renderItem}
        />
      );
}




  


export default AccountDetails