import React, {useContext, useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native'
import {AppContext} from '../../store/hooks/AppContext';
import {Service} from '../../types/api/service';
import {BankCard} from '../card/BankCard';
import {NavigationScreenProp, NavigationState, NavigationParams} from 'react-navigation';

interface Props {
    navigation: NavigationScreenProp<NavigationState, NavigationParams>;
}

const AccountSummary = (props: Props) => {
    const [accounts, setAccounts] = useState<Account[]>([]);
    const [, setResult] = useState<Service<Account[]>>({
        status: 'loading'
    });
    const monContext = useContext(AppContext);


    const onShowDetails = (account: Account) => {
        props.navigation.navigate('AccountDetails', {
            account
        });
    }

    useEffect(() => {
            const abort = new AbortController();
            const signal = abort.signal;
            if (monContext.logged && monContext.user && monContext.user.token && monContext.refreshAccount) {
                fetch('https://zandalar.dimitri-berger.fr/account', {
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': "Bearer ".concat(monContext.user.token)
                    },
                    signal: signal
                })
                    .then(response => response.json())
                    .then(response => {
                        setResult({status: 'loaded', payload: response});
                        monContext.fillAccountsData(response);
                        setAccounts(response);
                        monContext.refreshAccountList();
                    })
                    .catch(error => console.log(error.message));
            }

            return () => {
                abort.abort();
                monContext.refreshAccountList();
            }
        }, [monContext.refreshAccount]
    );

    return (
        <View style={styles.bankCardRow}>
            {accounts.map((account) => <BankCard key={account.id} item={account} onShowDetails={onShowDetails}/>)}
        </View>
    )
}


const styles = StyleSheet.create({
    bankCardRow: {
        flex: 1,
        flexDirection: "column",
        alignItems: "stretch",
        marginTop: 20
    },
});


export default AccountSummary
