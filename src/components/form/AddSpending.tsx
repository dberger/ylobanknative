import React, { useState, useEffect, useContext, Fragment } from 'react';
import { Input, Select, SelectOption, Button, Layout} from '@ui-kitten/components';
import {StyleSheet, View, Picker, BackHandler} from 'react-native'
import { CreditCardIconOutline } from '../../icons/icons';
import { AppContext } from '../../store/hooks/AppContext';
import { TransactionType } from '../../types/model/TransactionType';

const AddSpending = (props) => {
    const [amount, setAmount] = useState<string>('0.00');
    const [accountId, setAccountId] = useState<SelectOption>();
    const [spendingTypes, setspendingTypes] = useState<TransactionType[]>([])
    const [transactionTypeId, setTransactionTypeId] = useState<SelectOption>();
    const monContext = useContext(AppContext);
    const [formSubmitted, setFormSubmitted] = useState<boolean>(false);
    
    // Overide back button
    BackHandler.addEventListener('hardwareBackPress', () => props.navigation.goBack());
    
    const onFormSubmit = (): void => {
        setFormSubmitted(true);
    };
    useEffect(() => {
        const abort = new AbortController();
        const signal = abort.signal;
        if (monContext.logged && monContext.user && monContext.user.token) {
            if(!formSubmitted){

                fetch('https://zandalar.dimitri-berger.fr/transaction-type/debit', {
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': "Bearer ".concat(monContext.user.token) 
                    },
                    signal: signal
                })
                    .then(response => response.json())
                    .then(response => {
                        setspendingTypes(response);
                    })
                    .catch(error => console.log(error.message));
            } else {
                const data = new FormData();
                data.append("account", accountId.value);
                data.append("type", transactionTypeId.value);
                data.append("amount", amount);
                fetch('https://zandalar.dimitri-berger.fr/transaction/add/', {
                    method: 'POST',
                    headers: {
                        'Authorization': "Bearer ".concat(monContext.user.token) 
                    },
                    body: data,
                    signal: signal
                })
                    .then(response => {
                        props.navigation.goBack();
                        setFormSubmitted(false)
                        monContext.refreshAccountList();
                        // popup idea
                    })
                    .catch(error => console.log(error.message));
                
            }
        }
        return () => {
            abort.abort();
        }
    }, [formSubmitted])

    return (
        <Fragment>
            <Layout style={styles.formContainer}>
                <Fragment>
                    <Select
                        style={styles.formControl}
                        data={monContext.accounts.map((item, key) => {return {text: `${item.type} ${item.solde} €`, value: item.id}})}
                        selectedOption={accountId}
                        onSelect={setAccountId}
                        placeholder='Choisir un compte'
                    />
                    
                    <Select
                        style={styles.formControl}
                        data={spendingTypes.map((item, key) => {return {text: item.name, value: item.id}})}
                        selectedOption={transactionTypeId}
                        onSelect={setTransactionTypeId}
                        placeholder='Choisir un type de dépense'
                    />
                    <Input
                        style={styles.formControl}
                        placeholder='Montant'
                        value={amount.toString()}
                        onChangeText={(value) => setAmount(value)}
                        icon={CreditCardIconOutline}
                    />
                    <Button
                        style={styles.submitButton}
                        onPress={onFormSubmit}>
                            Enregistrer
                    </Button>
                </Fragment>
            </Layout>
        </Fragment>
    );
}

const styles = StyleSheet.create({
    formContainer: {
        flex: 1,
        paddingVertical: 16,
        paddingHorizontal: 16,
        justifyContent: 'center',
    },
    formControl: {
        marginVertical: 4
    },
    submitButton: {
        marginVertical: 24
    },
});

  


export default AddSpending
