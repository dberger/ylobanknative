import React, {useContext, useEffect, useState} from 'react';
import {
    ImageBackground,
    StyleSheet,
    View
} from 'react-native';
import {
    Button,
    CheckBox,
    Layout,
    Input, Text
} from '@ui-kitten/components';

import {
    EyeIcon,
    EyeOffIcon,
    PersonIcon
} from '../../icons/icons';
import {AppContext} from '../../store/hooks/AppContext';
import {Service} from '../../types/api/service';
import {User} from '../../types/model/User';
import {
    NavigationParams,
    NavigationScreenProp,
    NavigationState
} from 'react-navigation';

type Props = {
    navigation: NavigationScreenProp<NavigationState, NavigationParams>;
};

/**
 * LoginScreen
 */
const LoginComponent = (props: Props) => {
    const [shouldRemember, setShouldRemember] = useState<boolean>(false);
    const [passwordVisible, setPasswordVisible] = useState<boolean>(false);
    const [email, setEmail] = useState<string>('dimitri@dimitri-berger.fr');
    const [password, setPassword] = useState<string>('dimitri');
    const monContext = useContext(AppContext);
    const onFormSubmit = (): void => {
        monContext.login();
    };
    const [, setResult] = useState<Service<User>>({
        status: 'loading'
    });

//   const navigateHome = (): void => {
//   };

    useEffect(() => {
            if (monContext.logged) {
                fetch('https://zandalar.dimitri-berger.fr/rest/login', {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        username: email,
                        password: password
                    })
                })
                    .then(response => response.json())
                    .then(response => {
                        setResult({status: 'loaded', payload: response});
                        monContext.fillUser(email, response.token);
                        props.navigation.navigate('Dashboard');
                    })
                    .catch(error => console.log(error.message));
            }
        }, [monContext.logged]
    );

    const navigateSignUp = (): void => {
    };

    const navigateResetPassword = (): void => {
    };

    const onPasswordIconPress = (): void => {
        setPasswordVisible(!passwordVisible);
    };

    return (
        <React.Fragment>
            <ImageBackground
                style={styles.appBar}
                source={require('../../../assets/images/login-bg.jpg')}
            />
            <Layout style={styles.formContainer}>
                <React.Fragment>
                    <Text>{monContext.logged ? 'Connecté en tant que : ' + monContext.user.email : 'Non connecté'}</Text>
                    <Input
                        style={styles.formControl}
                        placeholder='Email'
                        keyboardType='email-address'
                        icon={PersonIcon}
                        onChangeText={(value) => setEmail(value)}
                        value={'dimitri@dimitri-berger.fr'}
                    />
                    <Input
                        style={styles.formControl}
                        placeholder='Mot de passe'
                        secureTextEntry={!passwordVisible}
                        onIconPress={onPasswordIconPress}
                        icon={passwordVisible ? EyeIcon : EyeOffIcon}
                        onChangeText={(value) => setPassword(value)}
                        value={'dimitri'}
                    />
                    <View style={styles.resetPasswordContainer}>
                        <CheckBox
                            style={styles.formControl}
                            checked={shouldRemember}
                            onChange={setShouldRemember}
                            text='Se souvenir de moi'
                        />
                        <Button
                            appearance='ghost'
                            status='basic'
                            onPress={navigateResetPassword}>
                            J'ai oublié mon mot de passe
                        </Button>
                    </View>
                    <Button
                        style={styles.submitButton}
                        onPress={onFormSubmit}>
                        Se connecter
                    </Button>
                </React.Fragment>
                <Button
                    appearance='ghost'
                    status='basic'
                    onPress={navigateSignUp}>
                    Vous n'avez pas de compte ?
                </Button>
            </Layout>
        </React.Fragment>
    );
};

const styles = StyleSheet.create({
    appBar: {
        height: 190
    },
    formContainer: {
        flex: 1,
        paddingVertical: 16,
        paddingHorizontal: 16
    },
    resetPasswordContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    formControl: {
        marginVertical: 4
    },
    submitButton: {
        marginVertical: 24
    },
    noAccountButton: {
        alignSelf: 'center'
    }
});

export default LoginComponent;
