import { Transaction } from "./Transaction";

export interface Account {
    id: number;
    type: string;
    solde: number;
    codeType: string;
    transactions: Transaction[];
}
