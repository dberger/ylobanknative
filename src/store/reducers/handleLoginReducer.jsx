const initialState = {};

export default function handleLogin(state = initialState, action) {
    let nextState;
    if (action.type === 'HANDLE_LOGIN') {
        nextState = {
            ...state,
            user: action.data
        };
        return nextState
    } else {
        return state
    }
}
