import { combineReducers } from 'redux'
import handleLoginReducer from './handleLoginReducer'

const allReducers = combineReducers({
    handleLoginReducer,
});

export default allReducers
