import React, {useState} from 'react';

export const AppContext = React.createContext({
    logged: false,
    refreshAccount: false,
    login: () => { },
    logout: () => {},
    user: {email: '', token: '', password: ''},
    fillUser: (email: string, token: string) => {console.log(email, token); },
    fillAccountsData: (accounts:Account[]) => {},
    accounts: [],
    refreshAccountList: () => {}
});

// @ts-ignore
export function AppContextProvider({ children }) {
    const [logged, setLogged] = useState(false);
    const [user, setUser] = useState({email: '', token: '', password: ''});
    const [accounts, setAccounts] = useState([]);
    const [refreshAccount, setRefreshAccount] = useState(true);

    function login() {
        setLogged(true);
    }
    function logout() {
        setLogged(false);
    }
    function refreshAccountList() {
        setRefreshAccount(!refreshAccount);
    }
    function fillUser(email: string, token: string) {
        setUser( {
            email: email,
            token: token,
            password: ''
        });
    }
    function fillAccountsData(accounts:Account[]){
        setAccounts(accounts);
    }
    const state = {
        logged,
        login,
        logout,
        user,
        accounts,
        fillUser,
        fillAccountsData,
        refreshAccountList,
        refreshAccount
    };

    return <AppContext.Provider value={state}>{children}</AppContext.Provider>;
}
