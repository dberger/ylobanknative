import React from 'react';

import LoginComponent from '../../components/login/login';
import {NavigationStackScreenProps} from 'react-navigation-stack';

type Props = {
    navigation: NavigationStackScreenProps;
};

/**
 * LoginScreen
 */
const LoginScreen = (props: Props) => {

    // @ts-ignore
    return <LoginComponent navigation={props.navigation}/>;
};

export default LoginScreen;
