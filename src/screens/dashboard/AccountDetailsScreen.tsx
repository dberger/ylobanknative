import React, {useContext} from 'react';
import {Text, Layout, withStyles} from '@ui-kitten/components';
import {AppContext} from '../../store/hooks/AppContext';
import AccountSummary from '../../components/account/accountSummary';
import AccountDetails from '../../components/account/accountDetails';

/**
 * AccountDetailsScreen
 */
const AccountDetailsLayout = (props) => {
        return (
    
    // 3. Access style prop with `themedStyle` prop
    <Layout style={props.themedStyle.container}>
            <AccountDetails account={props.navigation.state.params.account}/>
    </Layout>
  )};
    
  const AccountDetailsScreen = withStyles(AccountDetailsLayout, theme => ({
    container: {
      flex: 1,
    }
  }));

export default AccountDetailsScreen;
