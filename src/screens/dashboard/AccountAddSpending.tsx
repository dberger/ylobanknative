import React from 'react';
import {Layout, withStyles} from '@ui-kitten/components';
import AddSpending from '../../components/form/AddSpending';

/**
 * AccountAddSpendingScreen
 */
const AccountAddSpendingLayout = (props) => {
    return (
        <Layout style={props.themedStyle.container}>
            <AddSpending {...props}/>
        </Layout>
    )
};

const AccountAddSpendingScreen = withStyles(AccountAddSpendingLayout, theme => ({
    container: {
        flex: 1,
    }
}));

export default AccountAddSpendingScreen;
