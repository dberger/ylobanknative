import React, {useContext} from 'react';
import {Text, Layout, withStyles} from '@ui-kitten/components';
import {AppContext} from '../../store/hooks/AppContext';
import AccountSummary from '../../components/account/accountSummary';

/**
 * DashboardScreen
 */
const Dashboard = (props) => {
        return (
    
    // 3. Access style prop with `themedStyle` prop
    <Layout style={props.themedStyle.container}>
            <AccountSummary navigation={props.navigation}/>
    </Layout>
  )};
  
  // 1. Wrap it into `withStyles`
  
  const DashboardScreen = withStyles(Dashboard, theme => ({
    container: {
      flex: 1,
    }
  }));

export default DashboardScreen;
