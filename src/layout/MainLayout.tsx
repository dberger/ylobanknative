import React from 'react';

import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import LoginScreen from '../screens/login/LoginScreen';
import DashboardScreen from '../screens/dashboard/DashboardScreen';
import AccountDetailsScreen from '../screens/dashboard/AccountDetailsScreen';

const MainNavigator = createStackNavigator({
    Login: {
        screen: LoginScreen,
        navigationOptions: {
            title: 'Home',
            headerShown: false
        }},
    Dashboard: {
        screen: DashboardScreen,
        navigationOptions: {
            headerTitle: 'Tableau de bord'
        }
    },
    AccountDetails: {
        screen: AccountDetailsScreen,
        navigationOptions: {
            headerTitle: 'Détails des transactions'
        }
    }
});

const AppNavigatorContainer = createAppContainer(MainNavigator);

export default function MainLayout() {
    return (
        <AppNavigatorContainer/>
    );
}
