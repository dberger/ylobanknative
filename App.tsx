import React from 'react';
import { createAppContainer } from 'react-navigation';
import { SafeAreaView, ScrollView, StatusBar } from 'react-native';
import MainLayout from './src/layout/MainLayout';
import { createDrawerNavigator, DrawerItems } from 'react-navigation-drawer';
import AccountAddSpendingScreen from './src/screens/dashboard/AccountAddSpending';
import {Provider} from 'react-redux';
import Store from './src/store/configureStore';
import {AppContextProvider} from './src/store/hooks/AppContext';
import { IconRegistry, ApplicationProvider } from '@ui-kitten/components';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import {dark as darkTheme, mapping} from '@eva-design/eva';


export default class App extends React.Component {
    render() {
        return (
            <AppContextProvider>
                <Provider store={Store}>
                    <IconRegistry icons={EvaIconsPack}/>
                    <ApplicationProvider mapping={mapping} theme={darkTheme}>
                        <MainContainer/>
                    </ApplicationProvider>
                </Provider>
            </AppContextProvider>
        );
    }
}


const CustomDrawerComponent = (props) => (
    <ScrollView>
        <SafeAreaView>
            <StatusBar hidden={true} />
            <ScrollView>
                <DrawerItems {...props}/>
            </ScrollView>
        </SafeAreaView>
    </ScrollView>
)

const AppDrawerNavigator = createDrawerNavigator({
    Home: {
        screen: MainLayout,
        navigationOptions:{
            drawerLabel: 'Mes comptes'
        }
    },
    AddSpending: {
        screen: AccountAddSpendingScreen,
        navigationOptions:{
            drawerLabel: 'Ajouter un transfert d\'argent',
        }
    }
}, {
    contentComponent: CustomDrawerComponent
});

const MainContainer = createAppContainer(AppDrawerNavigator);
