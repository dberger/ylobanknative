module.exports = function(api) {
  api.cache(true);
  return {
    presets: ['babel-preset-expo'],
    ignore: [/[\/\\]core-js/, /@babel[\/\\]runtime/],
  };
};
